我们的目标是打造更强大、更灵活、更易用的商城系统，最终成为 java 程序员的首选商城框架，让您的商城开发事半功倍！我们具有多元的商业模式满足您任何使用场景的需求，有S2B2C供应链商城、B2B2C多商户商城、O2O外卖商城、B2C单商户商城、社区团购、B2B批发商城等众多商业模式并含有限时秒杀、直播、优惠券、满减、砍价、分销、套餐、拼团、消费返利、平台抽佣、储值、同城配送、到店自提、库存、代销更有强大的DIY装修功能。
✨ 采用JAVA JDK17搭配VUE3丶TS的商城系统
     在此特邀您的加入！Smart Shop商城系统助您开启商业新篇章，快速构筑商城梦。
<br/>
![输入图片说明](image/3.jpg)


![输入图片说明](image/1.jpg)
![输入图片说明](image/2.jpg)

我们的目标是打造更强大、更灵活、更易用的商城系统，最终成为 java 程序员的首选商城框架，让您的商城开发事半功倍！我们具有多元的商业模式满足您任何使用场景的需求，有S2B2C供应链商城、B2B2C多商户商城、O2O外卖商城、B2C单商户商城、社区团购、B2B批发商城等众多商业模式并含有限时秒杀、直播、优惠券、满减、砍价、分销、套餐、拼团、消费返利、平台抽佣、储值、同城配送、到店自提、库存、代销更有强大的DIY装修功能。
✨ 采用JAVA JDK17搭配VUE3丶TS的商城系统
     在此特邀您的加入！Smart Shop商城系统助您开启商业新篇章，快速构筑商城梦。
<br/>
![输入图片说明](image/3.jpg)



# 郑州程序员群

![输入图片说明](image/image.jpg)

# 重要重要通知：强烈欢迎你的参与和贡献，保姆式引导你参与lenosp项目开源，联系组织管理，感兴趣的话非常欢迎你的加入，技术无所谓。真真的！！
<p align="center">
	👉 <a target="_blank" href="http://42.192.219.164">http://42.192.219.164</a>👈|admin|123456
</p>
<p align="center">
	<a target="_blank" href="https://gitee.com/zzdevelop/lenosp/stargazers">
		<img src="https://gitee.com/zzdevelop/lenosp/badge/star.svg?theme=gvp" />
	</a>
        <a target="_blank" href="https://www.apache.org/licenses/LICENSE-2.0">
		<img src="https://img.shields.io/:license-apache2-read.svg" />
	</a>
        <a target="_blank" href="https://gitee.com/zzdevelop/lenosp">
		<img src="https://img.shields.io/badge/-Spring%20boot-green" />
	</a>
        <a target="_blank" href="https://gitee.com/zzdevelop">
		<img src="https://img.shields.io/badge/%E7%BB%84%E7%BB%87-%E9%83%91%E5%B7%9E%E7%A8%8B%E5%BA%8F%E5%91%98-green" />
	</a>

</p>

# 简介

lenosp是一个基于spring boot的脚手架，并提供完善社区文档教程，中小企业可以用来快速迭代。

您可以阅读文档快速部署上手：
<a target="_blank" href="https://www.kancloud.cn/zhuxm/zzdevelop/content">
**社区文档**
</a>
<a target="_blank" href="https://www.kancloud.cn/zhuxm/zzdevelop/3142792">
**项目模块介绍**
</a>
<a target="_blank" href="https://www.kancloud.cn/zhuxm/zzdevelop/3142806">
**本地开发部署教程**
</a>
<a target="_blank" href="https://www.kancloud.cn/zhuxm/zzdevelop/3143263">
**工具集**
</a>

# 体验

[http://42.192.219.164/login](http://42.192.219.164/login)
体验账户：admin，所有账户密码：123456
测试环境，所有业务增删改不会持久化，默认显示成功


# 模块

![输入图片说明](image/moduleimage.png)

# 里程碑

完善当前单体应用，趋于稳定，可供中小企业快速迭代。
- 代码风格约束
- 完善注释
- 模块划分调整
- bug&漏洞、pom升级
- 前端优化
- 通用代码生成功能
- 文档工具集教程
- 安装部署教程
- pom版本抽离
- 接口返回值完善封装
- 模块(maven)命名合理性
- 前端js方法、封装

# 关于我们

本项目是郑州程序员组织成员开发迭代

# 加入开源组织

![输入图片说明](image/image.jpg)

# 社区

文档地址：https://www.kancloud.cn/zhuxm/zzdevelop/3141950